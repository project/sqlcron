<?php

/**
 * @file
 * Admin page callbacks for the sqlcron module.
 */

/**
 * Statements/list tab.
 */
function sqlcron_admin_statements() {
  $result = db_query('SELECT sid, name, sql_text FROM {sqlcron} ORDER BY sid');
  $rows = array();
  while ($statement = db_fetch_object($result)) {
    $rows[] = array($statement->name, $statement->sql_text, l(t('edit'), 'admin/build/sqlcron/edit/'. $statement->sid), l(t('delete'), 'admin/build/sqlcron/delete/'. $statement->sid));
  }
  $header = array(t('Name'), t('SQL Statement'), array('data' => t('Operations'), 'colspan' => 2));

  return theme('table', $header, $rows);
}

/**
 * Statements/status tab.
 */
function sqlcron_status_statements() {
  $result = db_query('SELECT sid, name, result, lastrun FROM {sqlcron} ORDER BY sid');
  $rows = array();
  while ($statement = db_fetch_object($result)) {
    if ($statement->lastrun == 0)
    {
      $text_lastrun = "Never Run";
      $text_result = "Never Run";
    } else if ($statement->result == 1)
    {
      $text_lastrun = date('Y-m-d H:i:s', $statement->lastrun);
      $text_result = "Success";
    } else
    {
      $text_lastrun = date('Y-m-d H:i:s', $statement->lastrun);
      $text_result = "Failed";
    }
   
    $rows[] = array($statement->name, $text_result, $text_lastrun, l(t('edit'), 'admin/build/sqlcron/edit/'. $statement->sid), l(t('delete'), 'admin/build/sqlcron/delete/'. $statement->sid));
  }
  $header = array(t('Name'), t('Result'), t('Last Run'), array('data' => t('Operations'), 'colspan' => 2));

  return theme('table', $header, $rows);
}


/**
 * Statement edit page.
 */
function sqlcron_admin_edit($form_state = array(), $op, $sqlcron = NULL) {

  if (empty($sqlcron) || $op == 'add') {
    $sqlcron = array(
      'name' => '',
      'sql_text' => '',
      'sid' => NULL,
    );
  }
  $form['sqlcron_op'] = array('#type' => 'value', '#value' => $op);
  $form['name'] = array('#type' => 'textfield',
    '#title' => t('Name'),
    '#maxlength' => 255,
    '#default_value' => $sqlcron['name'],
    '#required' => TRUE,
  );
  $form['sql_text'] = array('#type' => 'textarea',
    '#title' => t('SQL Statement'),
    '#default_value' => $sqlcron['sql_text'],
    '#required' => TRUE,
  );
  $form['sid'] = array('#type' => 'value',
    '#value' => $sqlcron['sid'],
  );
  $form['submit'] = array('#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Validate the sqlcron statement edit page form submission.
 */
function sqlcron_admin_edit_validate($form, &$form_state) {
  if (empty($form_state['values']['name'])) {
    form_set_error('name', t('You must enter a name.'));
  }
  if (empty($form_state['values']['sql_text'])) {
    form_set_error('sql_text', t('You must enter one or more sql_text.'));
  }
}

/**
 * Process the sqlcron statement edit page form submission.
 */
function sqlcron_admin_edit_submit($form, &$form_state) {
  if (empty($form_state['values']['sid']) || $form_state['values']['sqlcron_op'] == 'add') {
    drupal_write_record('sqlcron', $form_state['values']);
    drupal_set_message(t('Statement %statement has been added.', array('%statement' => $form_state['values']['name'])));

  }
  else {
    drupal_write_record('sqlcron', $form_state['values'], 'sid');
    drupal_set_message(t('Statement %statement has been updated.', array('%statement' => $form_state['values']['name'])));
  }

  $form_state['redirect'] = 'admin/build/sqlcron';
  return;
}

/**
 * Statement delete page.
 */
function sqlcron_admin_delete(&$form_state, $sqlcron) {

  $form['sqlcron'] = array(
    '#type' => 'value',
    '#value' => $sqlcron,
  );

  return confirm_form($form, t('Are you sure you want to delete %statement?', array('%statement' => $sqlcron['name'])), 'admin/build/sqlcron', t('This action cannot be undone.'), t('Delete'), t('Cancel'));
}

/**
 * Process statement delete form submission.
 */
function sqlcron_admin_delete_submit($form, &$form_state) {
  $sqlcron = $form_state['values']['sqlcron'];
  db_query("DELETE FROM {sqlcron} WHERE sid = %d", $sqlcron['sid']);
  drupal_set_message(t('Statement %statement has been deleted.', array('%statement' => $sqlcron['name'])));

  $form_state['redirect'] = 'admin/build/sqlcron';
  return;
}
